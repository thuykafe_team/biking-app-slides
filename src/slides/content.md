# Biking-app

a RN app demonstrates authentication flow

---

## Sign In

     Email/Password Authentication verified 
     by http://localhost:5000

---

![Bigger-view](./media/out-2.gif)

---

## With via Social Network

    - [x] Faceobook
    - [x] Google
    - [ ] Twitter (TBD)

---

![Mobile-only](./media/out.gif)

 ---

## Register account
Create new account with contraints:

    - Email.regex() must true
    - Password.length must >= 8

---

![Bigger-view](./media/out-1.gif)

---

## Reset password
<!-- ![Cat](https://media.giphy.com/media/8vQSQ3cNXuDGo/giphy-downsized.gif) -->
    -  Send an email triggered from GET request
    -  User input new password in form POST